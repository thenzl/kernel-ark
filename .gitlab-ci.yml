# This CI will only work for project members. CI for public contributors
# runs via a webhook on the merge requests. There's nothing you have to do if
# you want your changes tested -- created pipeline will be automatically
# linked in the merge request and appropriate labels will be added to it.
# Changes to this file will NOT be reflected in the webhook testing.

include:
  - project: cki-project/pipeline-definition
    ref: main
    file: kernel_templates.yml

stages:
  - test
  - build
  - deploy

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'   # ARK release pipelines
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^cki-project.kernel-ark/'   # ARK MR CI
      variables:
        REQUESTED_PIPELINE_TYPE: 'trusted-ark'
    # We cannot merge rules through !reference and have to copy them over from the
    # CKI pipeline: https://gitlab.com/gitlab-org/gitlab/-/issues/322992
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.rhel/ || $CI_PROJECT_PATH =~ /^redhat.rhel/'
      variables:
        REQUESTED_PIPELINE_TYPE: 'internal'
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.centos-stream/ || $CI_PROJECT_PATH =~ /^redhat.centos-stream/'
      variables:
        REQUESTED_PIPELINE_TYPE: 'trusted'

# We need to differentiate between the pipeline types that are triggered from this
# single file. Once the mirroring to c9s stops this anchor can be dropped and the
# regular ".trusted" anchor and "trusted" type can be used. The template is added
# here instead of into CKI to avoid having to clear up two places after mirror drop.
.trusted-ark:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-trusted-contributors
    strategy: depend
  variables:
    PIPELINE_TYPE: 'trusted-ark'
    kernel_type: 'upstream'

.trigger_ark_pipeline:
  stage: test
  trigger:
    branch: kernel-ark
  variables:
    name: kernel-ark
    builder_image: registry.gitlab.com/cki-project/containers/builder-rawhide
    builder_image_tag: latest
    srpm_make_target: dist-srpm
    # We only need srpm so skip the rest of the stages
    skip_build: 'true'
    skip_publish: 'true'
    skip_test: 'true'
    skip_results: 'true'

.9-common:
  variables:
    builder_image_tag: latest
    srpm_make_target: dist-srpm
    build_kabi_stablelist: 'true'
    tree_yaml_name: rhel
    publish_elsewhere: 'true'
    native_tools: 'true'

.trigger_c9s_pipeline:
  trigger:
    branch: c9s
  variables:
    name: centos-stream-9
    builder_image: registry.gitlab.com/cki-project/containers/builder-stream9
    architectures: 'x86_64 ppc64le aarch64 s390x'
    kpet_tree_family: c9s

.trigger_rhel9_pipeline:
  trigger:
    branch: rhel9
  variables:
    name: rhel9
    builder_image: quay.io/cki/builder-rhel9
    architectures: 'x86_64 aarch64 s390x'   #ppc64le container is not available yet
    kpet_tree_family: rhel9

ark_merge_request:
  extends: [.trusted-ark, .merge_request, .trigger_ark_pipeline]

# Only enable c9s pipelines once we have the composes available in Beaker
# c9s_merge_request:
#   extends: [.trusted, .merge_request, .9-common, .trigger_c9s_pipeline]
# 
# c9s_baseline:
#   extends: [.trusted, .baseline, .9-common, .trigger_c9s_pipeline]

rhel9_merge_request:
  extends: [.internal, .merge_request, .9-common, .trigger_rhel9_pipeline]

rhel9_baseline:
  extends: [.internal, .baseline, .9-common, .trigger_rhel9_pipeline]


# scheduled job
.scheduled_setup:
  image: registry.gitlab.com/cki-project/containers/builder-fedora
  variables:
    GIT_DEPTH: "0"
    GIT_CLONE_PATH: $CI_BUILDS_DIR/$CI_CONCURRENT_ID/kernel-ark
  before_script:
    - echo "fastestmirror=true" >> /etc/dnf/dnf.conf
    - dnf -y install python3-gitlab git openssh-clients dnf-utils gnupg2
    - git config user.name "Fedora Kernel Team"
    - git config user.email "kernel-team@fedoraproject.org"
    - echo "$PYTHON_GITLAB_CONFIG" >> ~/.python-gitlab.cfg
    # Need SSH since the clone is set up without write access.
    - eval $(ssh-agent -s)
    - echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$GITLAB_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - git remote add gitlab git@gitlab.com:cki-project/kernel-ark.git
    - gpg2 --import "$TORVALDS_GPG_KEY"
    - git checkout --track origin/master && git describe
    - export PROJECT_ID="$CI_PROJECT_ID"

merge_upstream:
  extends: .scheduled_setup
  script:
    - git checkout --track origin/os-build && git describe
    - make dist-merge-upstream-push || exit 1
  retry: 2
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $RAWHIDE_RELEASE == "false"'

# scheduled job
rawhide_release:
  extends: .scheduled_setup
  script:
    - git checkout --track origin/ark-latest && git describe
    - git checkout --track origin/ark-infra && git describe
    - git checkout --track origin/os-build && git describe
    - redhat/scripts/ci/ark-create-release.sh "master" "$CI_PROJECT_ID" || exit_code=$?
    - if [ $exit_code -eq 3 ]; then echo "Tag exists, halting" && exit 0; fi;
    - if [ $exit_code -ne 0 ]; then echo "Unable to create release tag" && exit 1; fi;
    - git push gitlab os-build
    # ark-create-release.sh creates a tag for ark-latest to be pushed.
    # Make sure the ark-latest branch is checked out.
    - git checkout ark-latest
    - git push gitlab "$(git describe)"
    - git push gitlab ark/*
    - git push -f gitlab ark-latest
    - git push -f gitlab ark-infra
  retry: 2
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $RAWHIDE_RELEASE == "true"'

test_docs:
  stage: test
  image: registry.gitlab.com/cki-project/containers/python
  before_script:
    - python3 -m venv ~/docs-venv
    - source ~/docs-venv/bin/activate
    - pip install sphinx
    - cd redhat/docs/
  script: make SPHINXOPTS="-W" html
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "os-build"'
      changes:
        - redhat/docs/**/*


docs:
  stage: build
  image: registry.gitlab.com/cki-project/containers/python
  before_script:
    - python3 -m venv ~/docs-venv
    - source ~/docs-venv/bin/activate
    - pip install sphinx
    - cd redhat/docs/
  script: make SPHINXOPTS="-W" html
  artifacts:
    paths:
      - redhat/docs/_build/html/
  rules:
    - if: '$CI_COMMIT_BRANCH == "os-build"'
      changes:
        - redhat/docs/**/*

pages:
  stage: deploy
  image: registry.gitlab.com/cki-project/containers/python
  dependencies:
    - docs
  script:
    - mv redhat/docs/_build/html/ public/
  artifacts:
    paths:
      - public
  rules:
    - if: '$CI_COMMIT_BRANCH == "os-build"'
      changes:
        - redhat/docs/**/*
